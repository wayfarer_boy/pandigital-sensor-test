import fulltilt from './fulltilt';
import './app.css';
console.log(fulltilt);

let orientation;
let motion;

// Set up objects to request data whenever you need it

fulltilt.getDeviceOrientation({ type: 'game' }).then((_orientation) => {
  orientation = _orientation;
});

fulltilt.getDeviceMotion({ type: 'game' }).then((_motion) => {
  motion = _motion;
});

function getValues(time) {
  if (orientation && motion) {
    // alpha, beta, gamma are in degrees, between -180 and 180
    const { alpha, beta, gamma } = orientation.getScreenAdjustedEuler();
    // x, y, z are acceleration in metres per second
    const { x, y, z } = motion.getScreenAdjustedAcceleration();
    // Print values to the console
    console.log(time, alpha, beta, gamma, x, y, z);
  }
  window.requestAnimationFrame(getValues);
}

// This call is similar to Processing's draw function
// It runs the getValues function as fast as possible, without
// reducing the browser's framerate
window.requestAnimationFrame(getValues);
